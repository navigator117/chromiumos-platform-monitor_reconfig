// Copyright (c) 2010 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef MONITOR_RECONFIGURE_MONITOR_RECONFIGURE_MAIN_H_
#define MONITOR_RECONFIGURE_MONITOR_RECONFIGURE_MAIN_H_

#include <map>
#include <string>
#include <vector>

#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>

#include "monitor_reconfig/resolution_selector.h"

namespace monitor_reconfig {

// MonitorReconfigureMain is the class responsible for setting the external
// monitor to the max resolution based on the modes supported by the native
// monitor and the external monitor.
class MonitorReconfigureMain {
 public:
  // |window| is the root window.
  MonitorReconfigureMain(Display* display,
                         Window window,
                         XRRScreenResources* screen_info);
  ~MonitorReconfigureMain() {
    XRRFreeOutputInfo(lcd_output_info_);
    XRRFreeOutputInfo(external_output_info_);
  }

  // Main entry point.
  void Run();

 private:
  // Initializes the |lcd_output_| and |external_output_| members.
  void DetermineOutputs();

  // Returns whether an external monitor is connected.
  bool IsExternalMonitorConnected();

  // Sorts |output_info|'s modes by decreasing number of pixels, storing the
  // results in |modes_out|.
  void SortModesByResolution(RROutput output,
                             std::vector<ResolutionSelector::Mode>* modes_out);

  // Set the resolution for a particular device or for the screen.
  bool SetDeviceResolution(RROutput output,
                           const XRROutputInfo* output_info,
                           const ResolutionSelector::Mode& resolution);
  bool SetScreenResolution(const ResolutionSelector::Mode& resolution);

  // Disable output to a device.
  bool DisableDevice(RROutput output,
                     const XRROutputInfo* output_info);

  // Mapping between mode XIDs and mode information structures.
  std::map<int, XRRModeInfo*> mode_map_;

  // X Resources needed between functions.
  Display* display_;
  Window window_;
  XRRScreenResources* screen_info_;

  RROutput lcd_output_;
  XRROutputInfo* lcd_output_info_;

  RROutput external_output_;
  XRROutputInfo* external_output_info_;
};

}  // namespace monitor_reconfig

#endif  // MONITOR_RECONFIGURE_MONITOR_RECONFIGURE_MAIN_H_
