#!/bin/sh

# Copyright (c) 2010 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

export DISPLAY=:0
export XAUTHORITY=/home/chronos/.Xauthority
/usr/bin/monitor_reconfigure
