# Copyright (c) 2010 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

CXX ?= g++
CXXFLAGS ?= -Wall -Werror -g
PKG_CONFIG ?= pkg-config

LIBS = -lbase -lpthread -lrt
TEST_LIBS = -lgtest -lgflags
INCLUDE_DIRS = -I.. $(shell $(PKG_CONFIG) --cflags x11 xrandr)
LIB_DIRS = $(shell $(PKG_CONFIG) --libs x11 xrandr)


BIN=monitor_reconfigure
OBJECTS=monitor_reconfigure_main.o resolution_selector.o

.cc.o:
	$(CXX) $(CXXFLAGS) $(INCLUDE_DIRS) -c $< -o $@

$(BIN): $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(INCLUDE_DIRS) $(LIB_DIRS) $^ $(LIBS) $(LDFLAGS) \
	  -o $@

all: $(BIN)

test: resolution_selector_test

resolution_selector_test: resolution_selector_test.cc resolution_selector.o
	$(CXX) $(CXXFLAGS) $(INCLUDE_DIRS) $(LIB_DIRS) $^ \
	  $(LIBS) $(TEST_LIBS) $(LDFLAGS) -o $@

clean:
	@rm -f $(BIN) $(OBJECTS) resolution_selector_test
